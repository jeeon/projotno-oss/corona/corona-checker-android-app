# Covid 19 Checker
An app to check basic symptom of Novel Corona Virus

## Prerequisites

- Learn about [Git](https://git-scm.com/)
  - Use Git (latest)
- Learn about [Android](https://developer.android.com/docs)
  - Use Android Jetpack
- Learn about [GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/)

## Set Up
- Download latest `Android Stdio (3.+.+)`
- Clone the project from `Gitlab`
- Open the project in `Android Stdio`

### Development

### Production

## Git
- Push to feature branches **NNN-feature-branch-name** to build and test.
- Merge and push to branch **master**  to build, test, and *deploy* to ****

## Common issue