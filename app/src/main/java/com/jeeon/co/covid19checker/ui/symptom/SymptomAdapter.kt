package com.jeeon.co.covid19checker.ui.symptom

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jeeon.co.covid19checker.data.network.model.SymptomResponse
import com.jeeon.co.covid19checker.databinding.ListItemSymptomBinding

class SymptomAdapter constructor(
    private val symptomClickCallback: SymptomClickCallback
) : ListAdapter<SymptomResponse, SymptomAdapter.SymptomViewHolder>(SymptomDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SymptomViewHolder {
        val binding =
            ListItemSymptomBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SymptomViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SymptomViewHolder, position: Int) {
        val symptom = getItem(position)
        symptom ?: return

        holder.apply {
            bind(createOnClickListener(symptom), symptom)
            itemView.tag = symptom
        }
    }

    private fun createOnClickListener(symptom: SymptomResponse): View.OnClickListener {
        return View.OnClickListener {
            symptomClickCallback.onSymptomItemClick(symptom)
        }
    }

    class SymptomViewHolder(private val binding: ListItemSymptomBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var symptom: SymptomResponse

        fun bind(listener: View.OnClickListener, item: SymptomResponse) {
            symptom = item
            binding.apply {
                clickListener = listener
                symptom = item
                executePendingBindings()
            }
        }
    }
}