package com.jeeon.co.covid19checker.ui.symptom

import com.jeeon.co.covid19checker.data.network.model.SymptomResponse


interface SymptomClickCallback {
    fun onSymptomItemClick(symptom: SymptomResponse)
}