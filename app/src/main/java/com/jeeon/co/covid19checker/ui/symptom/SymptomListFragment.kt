package com.jeeon.co.covid19checker.ui.symptom

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.jeeon.co.covid19checker.data.network.model.SymptomResponse
import com.jeeon.co.covid19checker.databinding.FragmentSymptomListBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class SymptomListFragment : Fragment(), SymptomClickCallback {

    private val symptomViewModel: SymptomViewModel by viewModel()
    private lateinit var binding: FragmentSymptomListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSymptomListBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = symptomViewModel

        val adapter = SymptomAdapter(this)
        binding.symptomList.adapter = adapter
        subscribeUI(adapter)

        return binding.root
    }

    private fun subscribeUI(symptomAdapter: SymptomAdapter) {
        symptomViewModel.symptomList.observe(viewLifecycleOwner, Observer {
            symptomAdapter.submitList(it)
        })
    }

    override fun onSymptomItemClick(symptom: SymptomResponse) {

    }
}
