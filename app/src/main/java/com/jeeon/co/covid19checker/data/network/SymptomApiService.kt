package com.jeeon.co.covid19checker.data.network

import com.jeeon.co.covid19checker.data.network.model.SymptomResponse
import io.reactivex.Observable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

const val API_KEY = ""
const val BASE_URL = ""


interface SymptomApiService {
    @GET("/")
    fun getSymptomList(): Observable<SymptomResponse>

    //FIXME: Need to make a separate reusable class
    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor
        ): SymptomApiService {
            val requestInterceptor = Interceptor { chain ->
                val url = chain.request()
                    .url()
                    .newBuilder()
                    .addQueryParameter("key", API_KEY)
                    .build()
                val request = chain.request()
                    .newBuilder()
                    .url(url)
                    .build()

                return@Interceptor chain.proceed(request)
            }

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
                .addInterceptor(connectivityInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(SymptomApiService::class.java)
        }
    }
}