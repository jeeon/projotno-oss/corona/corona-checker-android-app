import com.jeeon.co.covid19checker.ui.symptom.SymptomListFragment
import com.jeeon.co.covid19checker.ui.symptom.SymptomViewModel
import com.jeeon.co.covid19checker.ui.test.TestFragment
import com.jeeon.co.covid19checker.ui.test.TestViewModel
import com.jeeon.co.covid19checker.ui.safety.SafetyFragment
import com.jeeon.co.covid19checker.ui.safety.SafetyViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {
    // ViewModels
    viewModel { TestViewModel() }
    viewModel { SymptomViewModel() }
    viewModel { SafetyViewModel() }
    // Fragments
    factory { TestFragment() }
    factory { SymptomListFragment() }
    factory { SafetyFragment() }

}