package com.jeeon.co.covid19checker.ui.symptom

import androidx.recyclerview.widget.DiffUtil
import com.jeeon.co.covid19checker.data.network.model.SymptomResponse

class SymptomDiffCallback : DiffUtil.ItemCallback<SymptomResponse>() {

    override fun areItemsTheSame(oldItem: SymptomResponse, newItem: SymptomResponse): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: SymptomResponse, newItem: SymptomResponse): Boolean {
        return oldItem == newItem
    }
}