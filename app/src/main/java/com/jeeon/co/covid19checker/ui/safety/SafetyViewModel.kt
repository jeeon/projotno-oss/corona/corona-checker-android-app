package com.jeeon.co.covid19checker.ui.safety

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SafetyViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is safety Fragment"
    }
    val text: LiveData<String> = _text
}