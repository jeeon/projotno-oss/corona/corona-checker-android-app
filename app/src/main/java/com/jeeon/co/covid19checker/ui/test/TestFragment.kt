package com.jeeon.co.covid19checker.ui.test

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.jeeon.co.covid19checker.databinding.FragmentTestBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class TestFragment : Fragment() {

    private val testViewModel: TestViewModel by viewModel()
    private lateinit var binding: FragmentTestBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTestBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = lifecycleOwner
            viewModel = testViewModel
        }
        subscribeUi()

        return binding.root
    }

    private fun subscribeUi() {
        testViewModel.text.observe(viewLifecycleOwner, Observer {
            binding.textHome.text = it
        })
    }
}
