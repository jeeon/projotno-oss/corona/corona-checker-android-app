package com.jeeon.co.covid19checker.data.network.model

data class SymptomResponse(
    val id: String,
    val title: String,
    val message: String
)