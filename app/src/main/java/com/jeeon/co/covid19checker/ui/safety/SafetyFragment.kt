package com.jeeon.co.covid19checker.ui.safety

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.jeeon.co.covid19checker.databinding.FragmentSafetyBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class SafetyFragment : Fragment() {

    private val safetyViewModel: SafetyViewModel by viewModel()
    private lateinit var binding: FragmentSafetyBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSafetyBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = lifecycleOwner
            viewModel = safetyViewModel
        }
        subscribeUi()

        return binding.root
    }

    private fun subscribeUi() {
        safetyViewModel.text.observe(viewLifecycleOwner, Observer {
            binding.textNotifications.text = it
        })
    }
}
