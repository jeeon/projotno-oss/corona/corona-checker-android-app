package com.jeeon.co.covid19checker.data.repository

import androidx.lifecycle.LiveData


interface SafetyRepository {
    fun getSafetyList(): LiveData<Any>
}