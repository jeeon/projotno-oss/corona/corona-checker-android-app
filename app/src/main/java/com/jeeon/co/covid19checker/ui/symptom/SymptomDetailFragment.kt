package com.jeeon.co.covid19checker.ui.symptom

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.jeeon.co.covid19checker.R

/**
 * A simple [Fragment] subclass.
 */
class SymptomDetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_symptom_detail, container, false)
    }

}
