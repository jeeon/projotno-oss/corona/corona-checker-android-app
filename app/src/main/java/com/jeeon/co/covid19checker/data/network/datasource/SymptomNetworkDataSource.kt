package com.jeeon.co.covid19checker.data.network.datasource

import com.jeeon.co.covid19checker.data.network.model.SymptomResponse
import io.reactivex.Observable

interface SymptomNetworkDataSource {
    fun getSymptomList(): Observable<SymptomResponse>
}