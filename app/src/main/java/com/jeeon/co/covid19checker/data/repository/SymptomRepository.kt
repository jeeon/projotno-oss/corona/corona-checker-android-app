package com.jeeon.co.covid19checker.data.repository

import androidx.lifecycle.LiveData
import com.jeeon.co.covid19checker.data.network.model.SymptomResponse


interface SymptomRepository {
    fun getSymptomList(): LiveData<SymptomResponse>
}