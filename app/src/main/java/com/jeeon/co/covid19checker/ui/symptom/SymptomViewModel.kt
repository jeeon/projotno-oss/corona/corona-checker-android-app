package com.jeeon.co.covid19checker.ui.symptom

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jeeon.co.covid19checker.data.network.model.SymptomResponse

class SymptomViewModel : ViewModel() {

    private val tempSymptomList = listOf<SymptomResponse>(
        SymptomResponse("1", "Fever", "This is fever"),
        SymptomResponse("2", "Cough", "This is cough"),
        SymptomResponse("3", "Tiredness", "This is tiredness"),
        SymptomResponse("4", "Difficulty breathing", "This is difficulty breathing")
    )

    private val _symptomList = MutableLiveData<List<SymptomResponse>>().apply {
        value = tempSymptomList
    }
    val symptomList: LiveData<List<SymptomResponse>> = _symptomList
}